//
//  LikedViewController.swift
//  final
//
//  Created by Alikhan on 8/11/21.
//

import UIKit

class LikedViewController: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    

    public var medsLiked : [Meditation] = []
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        backButton.layer.cornerRadius = 20
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.register(UINib(nibName: MeditationTableViewCell.identifier, bundle: Bundle(for: MeditationTableViewCell.self)), forCellReuseIdentifier: MeditationTableViewCell.identifier)
        self.tableView.reloadData()
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}




extension LikedViewController : UITableViewDelegate{
    
}

extension LikedViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return medsLiked.count
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(self.medsLiked.count)
        let cell = tableView.dequeueReusableCell(withIdentifier: MeditationTableViewCell.identifier, for: indexPath) as! MeditationTableViewCell
        let mess =  medsLiked[indexPath.row]
        
        
        cell.selectionStyle = .none
        
        cell.med = mess
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let vc = storyboard?.instantiateViewController(identifier: "view") as? ViewController else{
            print("login to selection")
            return
        }
        vc.songName =  medsLiked[indexPath.row].sound_name
        
                
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
