//
//  ViewController.swift
//  final
//
//  Created by Alikhan on 8/9/21.
//

import UIKit
import AVFoundation
import SwiftGifOrigin

class ViewController: UIViewController {

    @IBOutlet weak var TimerLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var startStopButton: UIButton!
    
    
    
    
    
    var timer:Timer = Timer()
    var count:Int = 1800
    var timerCounting:Bool = true
    
    public var songName : String?
    var player: AVAudioPlayer?
    
    
    var images = [1: "waneella-Pixel-Art-Pixel-Gif-4453459",
                  2: "Pixel-Art-Pixel-Gif-kirokaze-длиннопост-3583982",
                  3: "Pixel-Art-Pixel-Gif-kirokaze-длиннопост-3583983",
                  4: "waneella-Pixel-Gif-Pixel-Art-4297304",
                  5: "waneella-Pixel-Gif-Pixel-Art-4816441",
                  6: "waneella-Pixel-Gif-Pixel-Art-4971086",
                  7: "waneella-Pixel-Gif-Pixel-Art-4976831",
                  8: "waneella-Pixel-Gif-Pixel-Art-5074898",
                  9: "гифки-Pixel-Art-kirokaze-песочница-2936252"
    ]
    
    
    
    func configure(){
        if let songName = self.songName{
            let url = Bundle.main.path(forResource: songName, ofType: "mp3")
            do {
                
                try AVAudioSession.sharedInstance().setMode(.default)
                try AVAudioSession.sharedInstance().setActive(true, options: .notifyOthersOnDeactivation)
                
                guard let url = url else {
                    print("url error")
                    return
                }
                
                self.player = try AVAudioPlayer(contentsOf: URL(string: url)!)
                
                
    
           
                
                
                player!.play()
                
                
            }catch{
                print("audio error ocuured")
            }
        }
       
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
            player!.stop()
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.contentMode = .scaleAspectFill
        let number = Int.random(in: 1...9)
        
        imageView.image = UIImage.gif(name: images[number]!)
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCounter), userInfo: nil, repeats: true)
        startStopButton.setTitleColor(UIColor.red, for: .normal)
        configure()
    }
    

    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    @objc func timerCounter() -> Void
    {
        if count - 1 == 0 {
            print("stop music return to new page")
            
            player!.stop()
            self.navigationController?.popViewController(animated: true)
            
           
        }else{
            count = count - 1
            let time = secondsToHoursMinutesSeconds(seconds: count)
            let timeString = makeTimeString(hours: time.0, minutes: time.1, seconds: time.2)
            TimerLabel.text = timeString
        }
    }
    
    func secondsToHoursMinutesSeconds(seconds: Int) -> (Int, Int, Int)
    {
        return ((seconds / 3600), ((seconds % 3600) / 60),((seconds % 3600) % 60))
    }
    
    
    func makeTimeString(hours: Int, minutes: Int, seconds : Int) -> String
    {
        var timeString = ""
        timeString += String(format: "%02d", hours)
        timeString += " : "
        timeString += String(format: "%02d", minutes)
        timeString += " : "
        timeString += String(format: "%02d", seconds)
        return timeString
    }

    
    
    
    @IBAction func startStopTapped(_ sender: Any) {
        if(timerCounting)
        {
            timerCounting = false
            timer.invalidate()
            startStopButton.setTitle("START", for: .normal)
            startStopButton.setTitleColor(UIColor.green, for: .normal)
        }
        else
        {
            timerCounting = true
            startStopButton.setTitle("STOP", for: .normal)
            startStopButton.setTitleColor(UIColor.red, for: .normal)
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerCounter), userInfo: nil, repeats: true)
        }
        
        
        
        if player?.isPlaying == true{
            player?.pause()
        }else{
            player?.play()
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


