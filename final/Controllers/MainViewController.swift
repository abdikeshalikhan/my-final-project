//
//  MainViewController.swift
//  final
//
//  Created by Alikhan on 8/11/21.
//

import UIKit
import SwiftGifOrigin

class MainViewController: UIViewController {
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var startButton: UIButton!
    
    @IBAction func getStartedButton(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(identifier: "division") as? DivisionViewController else{
            print("division")
            return
        }
        
                
        
        navigationController?.pushViewController(vc, animated: true)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        startButton.layer.cornerRadius = 20
        
        backgroundImage.image = UIImage.gif(name: "гифки-Pixel-Art-kirokaze-песочница-2936252")
    }
    
    
    

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    

}
