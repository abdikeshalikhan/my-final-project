//
//  DivisionViewController.swift
//  final
//
//  Created by Alikhan on 8/11/21.
//

import UIKit

class DivisionViewController: UIViewController {
    
    
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    
    
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerButton.layer.cornerRadius = 20
        loginButton.layer.cornerRadius = 20
        
        backgroundImage.image = UIImage.gif(name: "гифки-Pixel-Art-kirokaze-песочница-2936252")
        // Do any additional setup after loading the view.
    }
    @IBAction func registerButtonPressed(_ sender: Any) {
        guard let vc = storyboard?.instantiateViewController(identifier: "register") as? RegisterViewController else{
            print("register")
            return
        }
        
        
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(identifier: "login") as? LoginViewController else{
            print("login")
            return
        }

        
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
