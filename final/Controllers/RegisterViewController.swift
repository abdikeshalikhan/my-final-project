//
//  RegisterViewController.swift
//  final
//
//  Created by Alikhan on 8/11/21.
//

import UIKit
import SwiftGifOrigin
import Firebase


class RegisterViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var backButton: UIButton!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerButton.layer.cornerRadius = 20
        backButton.layer.cornerRadius = 20
        backgroundImage.image = UIImage.gif(name: "гифки-Pixel-Art-kirokaze-песочница-2936252")
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        
        guard let email = emailField.text else {return}
        guard let password = passwordField.text else {return}
        
        Auth.auth().createUser(withEmail: email, password: password) { [weak self] (result, error) in
            if let error = error {
                print(error)
            }else{
                
                guard let vc = self?.storyboard?.instantiateViewController(identifier: "selection") as? SelectionViewController else{
                    print("login to selection")
                    return
                }
                
                self?.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
