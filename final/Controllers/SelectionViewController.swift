//
//  SelectionViewController.swift
//  final
//
//  Created by Alikhan on 8/11/21.
//

import UIKit
import Firebase


class SelectionViewController: UIViewController, RefreshFavorites {
    
    @IBOutlet weak var backButton: UIButton!
    func refresh(_ med : Meditation) {
        self.medsLiked.append(med)
        print("sukas")
    }
    
    private let medsDB = Database.database().reference().child("Meds")
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var likedButton: UIButton!
    

    
    var meds : [Meditation] = []{
        didSet{
            self.tableView.reloadData()
        }
    }
    var medsLiked : [Meditation] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureMeds()
        print(self.meds.count)
        likedButton.layer.cornerRadius = 20
        backButton.layer.cornerRadius = 20
        tableView.delegate = self
        tableView.dataSource = self
        
        
        tableView.register(UINib(nibName: MeditationTableViewCell.identifier, bundle: Bundle(for: MeditationTableViewCell.self)), forCellReuseIdentifier: MeditationTableViewCell.identifier)
        self.tableView.reloadData()
    }
    
    
    
    func configureMeds(){
        let m1 = Meditation(name: "LOFI", sound_name: "lofi", backroundImage: "lofi")
        let m2 = Meditation(name: "RAIN", sound_name: "rain", backroundImage: "rain")
        let m3 = Meditation(name: "WATER", sound_name: "water", backroundImage: "water")
        let m4 = Meditation(name: "CLASSIC", sound_name: "classic", backroundImage: "music")
        let m5 = Meditation(name: "TRAIN", sound_name: "train", backroundImage: "train")
        let m6 = Meditation(name: "ASD", sound_name: "asd", backroundImage: "rain")
        
        self.meds.append(m1)
        self.meds.append(m2)
        self.meds.append(m3)
        self.meds.append(m4)
        self.meds.append(m5)
        self.meds.append(m6)
    }
    
    
    @IBAction func backButtonPressed(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func likedButtonPressed(_ sender: Any) {
        
        guard let vc = storyboard?.instantiateViewController(identifier: "liked") as? LikedViewController else{
            print("selection")
            return
        }
        
        vc.medsLiked = self.medsLiked
        
        navigationController?.pushViewController(vc, animated: true)
    }
    

    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}



extension SelectionViewController : UITableViewDelegate{
    
}

extension SelectionViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 120
        
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        print(self.meds.count)
        let cell = tableView.dequeueReusableCell(withIdentifier: MeditationTableViewCell.identifier, for: indexPath) as! MeditationTableViewCell
        
        let mess =  meds[indexPath.row]
        
        cell.delegate = self
        cell.selectionStyle = .none
        cell.med = mess
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let vc = storyboard?.instantiateViewController(identifier: "view") as? ViewController else{
            print("login to selection")
            return
        }
        vc.songName =  meds[indexPath.row].sound_name
        
                
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
