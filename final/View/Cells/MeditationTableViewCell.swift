//
//  MeditationTableViewCell.swift
//  final
//
//  Created by Alikhan on 8/11/21.
//

import UIKit
import Firebase

protocol RefreshFavorites{
    func refresh(_ med :Meditation)
}

class MeditationTableViewCell: UITableViewCell {

    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var backroundImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    public static let identifier = "MeditationTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        backroundImage.layer.cornerRadius = 20

    }
    
    var delegate: RefreshFavorites?
    

    var isSeleted : Bool? = false
    

    public var med : Meditation? = nil{
        didSet{
            if let med = med{
                backroundImage.image = UIImage(named: med.backroundImage)
                nameLabel.text = med.name
            }
        }
    }


    
    @IBAction func likeButtonPressed(_ sender: Any) {
        
        if !isSelected{
            if let mm = self.med{
                self.delegate?.refresh(mm)
            }
            
            likeButton.setImage(UIImage(systemName: "suit.heart.fill"), for: .normal)
        }else{
            likeButton.setImage(UIImage(systemName: "heart"), for: .normal)
        }
       
        isSelected = !isSelected
    }
    
    
}
